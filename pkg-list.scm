;; Copyright (C) 2010, 2011 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

;; You should have received a copy of the BSD license along with this
;; program. If not, see <http://www.debian.org/misc/bsd.license>.

(package (wak-fmt (0 8) (1))
  (depends
   (srfi-1)
   (srfi-6)
   (srfi-13)
   (srfi-69)
   (wak-common))
  
  (synopsis "formatting combinator library")
  (description
   "A library of procedures for formatting Scheme objects to text in"
   "various ways, and for easily concatenating, composing and extending"
   "these formatters efficiently without resorting to capturing and"
   "manipulating intermediate strings.")
  (homepage "http://synthcode.com/scheme/fmt/")
  
  (libraries
   (sls -> "wak")
   (("fmt" "private") -> ("wak" "fmt" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
