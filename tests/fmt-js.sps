#!r6rs
;; Copyright (C) 2011 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

;; You should have received a copy of the BSD license along with this
;; program. If not, see <http://www.debian.org/misc/bsd.license>.

(import (rnrs)
        (srfi :26 cut)
        (srfi :64 testing)
        (wak private include)
        (wak fmt)
        (wak fmt js))

(define-syntax test
  (syntax-rules ()
    ((test expected expr)
     (test-equal expected expr))))

(define-syntax cond-expand
  (syntax-rules ()
    ((cond-expand . args)
     (begin))))

(include-file ((wak fmt private) test-fmt-js))
